\chapter{Metodología}
\label{cap:Metodologia}

\drop{E}n el presente capítulo se describe la metodología de desarrollo utilizada para la elaboración de este \ac{tfg}. Al final del capítulo se incluirán las tecnologías y las herramientas \textit{software} y \textit{hardware} que han constituido el entorno de trabajo.  

\section{Metodología de Prototipado}
Se denomina Metodología de Prototipado \textit{Software} al proceso de desarrollo en el que se parte de una versión incompleta del sistema, conocida como \textit{prototipo}, que puede ejecutar y realizar funciones básicas y que evoluciona a través del desarrollo y análisis de nuevos prototipos (que incorporan mejoras o nuevas funcionalidades) hasta conseguir la versión final del sistema. Cada prototipo es presentado a los usuarios para que, mediante pruebas, se determine de forma más concreta cómo es el producto deseado y qué correcciones es necesario llevar a cabo para conseguirlo. A partir de los resultados de estas pruebas se decide si el producto se encuentra en su versión final o si se requiere de otra iteración y de la implementación de un nuevo prototipo. Esta metodología se aplica en diferentes campos de la ingeniería, incluido el campo del \textit{software}.

\begin{figure}[htb]
	\centering
	\includegraphics[height=6cm]{figs/metodologia/etapas_prototipado.pdf} 
	\caption{Etapas de la Metodología de Prototipado \textit{Software} \cite{campis2015excalibur}} 
	\label{fig:etapas_prototipado}
\end{figure}

El prototipo es una herramienta que sirve como mecanismo para identificar los requisitos del \textit{software}. Su construcción se lleva a cabo a través de las siguientes etapas (ver Figura \ref{fig:etapas_prototipado}): 
\begin{enumerate}
    \item \textbf{Recolección de requisitos:} el desarrollador y el cliente definen los objetivos globales y los objetivos más específicos que se desean conseguir con el prototipo. 
    \item \textbf{Diseño:} partiendo de los requisitos, se realiza un diseño rápido con funcionalidad centrado en los aspectos del \textit{software}, visibles para el usuario. 
    \item \textbf{Construcción:} en función del diseño, comienza la construcción del prototipo para poder mostrárselo al cliente lo antes posible. 
    \item \textbf{Evaluación:} se busca verificar que el prototipo ha sido construido conforme a los requisitos definidos, pudiendo añadir o modificar los requisitos planteados para el proyecto. Esta tarea la realizan el cliente y los usuarios, y permite la creación y el refinamiento de los requisitos \textit{software} del proyecto.
    \item \textbf{Refinamiento:} a partir de la evaluación del prototipo, este es refinado de forma iterativa hasta satisfacer las necesidades del cliente. Además permite que el desarrollador conozca cada vez mejor cómo debe ser el sistema. Esta etapa puede llevar a un posible rediseño del prototipo para su posterior reconstrucción y evaluación por parte del cliente. 
    \item \textbf{Producto final:} en caso de que no sea necesario refinar el producto y se haya cumplido con la especificación del cliente, se obtiene el producto final.
\end{enumerate}

El prototipado es una técnica de análisis que permite actualizar los requisitos funcionales del sistema \textit{software}. Por lo tanto, lo favorable es evolucionar el prototipo hasta obtener el producto final en lugar de deshacerlo y construir el producto partiendo desde cero. Para hacer esto posible, se repiten las etapas de la 2 a la 5 tantas veces como sea necesario. Es habitual tener que desechar la primera versión de cualquier sistema, motivo por el que la primera muestra del prototipo es intencionalmente imperfecta pero fácil de modificar y barata de producir. La mejora iterativa garantiza que el producto final se ajuste mejor a los requisitos del usuario. El ciclo de vida de un prototipo se muestra en la Figura \ref{fig:ciclo_vida_prototipo}.

\begin{figure}[htb]
	\centering
	\includegraphics[height=5.5cm]{figs/metodologia/ciclo_vida_prototipo.pdf} 
	\caption{Ciclo de vida del prototipo \cite{prototipado}} 
	\label{fig:ciclo_vida_prototipo}
\end{figure}

El uso de prototipos en el desarrollo de \textit{software} tiene diversas ventajas: 

\begin{itemize}
    \item Puede mejorar la calidad de los requisitos y de la especificación del sistema. La identificación temprana de las necesidades del cliente permite obtener \textit{software} más rápido de forma menos costosa. 
    
    \item Requiere la participación del usuario, por lo que al interactuar con el prototipo proporciona especificaciones más completas.
    
    \item Permite estimar y calcular plazos, así como el tiempo requerido, hasta alcanzar determinados hitos. 
    
    \item Evita malentendidos, siendo más probable que el producto final satisfaga las necesidades del usuario en apariencia, calidad y rendimiento.
    
    \item Hay indicios visibles de progreso. Los prototipos intermedios pueden ser utilizados cuando se demandan resultados y mayor velocidad de desarrollo. 
\end{itemize}

En Ingeniería del \textit{Software} el prototipo es aquella parte del modelo de la aplicación cuyo propósito es permitir que los usuarios evalúen las propuestas de los desarrolladores, probándolas con tiempo para afinar el diseño del producto final. Además, los usuarios finales pueden utilizar la creación de prototipos para describir requisitos que no se hayan considerado y que podrían ser un factor clave en la relación comercial existente entre el desarrollador y el usuario.


La razón primordial del uso de los prototipos es la reducción en tiempo y coste de la implementación del sistema. Esta reducción se puede conseguir mediante la supresión de ciertas características o simplificando ciertas funcionalidades del sistema. Existen dos dimensiones, denominadas horizontal y vertical \cite{nielsen}:
\begin{itemize}
    \item \textbf{Prototipado horizontal:} proporciona una amplia visión de todo un sistema o subsistema y se centra principalmente en la interacción con el usuario, por lo que incluye la interfaz pero no contiene funcionalidad subyacente. Esta dimensión es una simulación de la interfaz en la que no se puede realizar ningún trabajo real. 
    
     \item \textbf{Prototipado vertical:} se produce un sistema que presenta pocas características, pero la funcionalidad de estas está completamente implementada. Por tanto, el cliente puede probar una parte básica y limitada del sistema, pero funcional. 
\end{itemize}

Asimismo, las técnicas de prototipado suelen clasificarse de la siguiente manera \cite{rettig1994prototyping}:
\begin{itemize}
    \item \textbf{Prototipos de baja fidelidad:} implementan aspectos generales del sistema sin entrar en detalles específicos del mismo, permitiendo abarcar un mayor número de características. 
    \item \textbf{Prototipos de alta fidelidad:} representan aspectos más precisos del sistema que se desea conseguir, lo que permite conseguir una funcionalidad completa en partes concretas. 
\end{itemize}

Los prototipos de baja fidelidad se caracterizan por ser rápidos de construir y económicos, mientras que los prototipos de alta fidelidad se caracterizan por el uso de herramientas especializadas de prototipado que ofrecen mayor precisión y detalle (además de requerir más tiempo y suponer un mayor coste). Sin embargo, los prototipos de alta fidelidad no tienen por qué ser peores que los prototipos de baja fidelidad, ya que cada tipo tiene sus propias ventajas e inconvenientes. Por ejemplo, en ciertas pruebas de rendimiento se obtienen mejores resultados mediante los prototipos de alta fidelidad. 

\subsection{Tipos de prototipado \textit{software}}
Existen diversas variantes del modelo de prototipado \textit{software}, aunque están basados principalmente en los modelos de prototipado desechable y prototipado evolutivo \cite{smith1991software}. 

\begin{itemize}
    \item \textbf{Prototipado desechable:} prototipo con funcionalidad simulada que eventualmente se descartará en lugar de convertirse en parte del \textit{software} final. Una vez que se lleva a cabo la recopilación preliminar de requisitos, se construye un modelo de trabajo simple del sistema para mostrar al usuario cómo se vería el sistema final. Una vez aclarados los requisitos será cuando se complete la funcionalidad del sistema, dependiendo del último prototipo diseñado. El factor más importante de este tipo de prototipado es la rapidez con la que se proporciona el modelo. 
    \item \textbf{Prototipado evolutivo:} se parte de un prototipo robusto que se refina y reconstruye de forma iterativa hasta llegar al prototipo final. El prototipo inicial constituye el núcleo del sistema, añadiendo posteriormente las mejoras y los requisitos adicionales. En el presente \ac{tfg} se ha utilizado este enfoque, por lo que se explicará con mayor detalle más adelante (ver Sección \ref{proto_evolutivo}).  
    \item \textbf{Prototipado incremental:} se construyen prototipos separados que posteriormente se fusionarán para construir el producto final. Con ello se reduce la brecha de tiempo que existe entre el usuario y el desarrollador \textit{software}. 
    \item \textbf{Prototipado extremo:} como proceso de desarrollo, este enfoque es usado principalmente en el desarrollo web. El desarrollo web se divide en tres fases, cada una de ellas basada en la fase anterior:
    \begin{enumerate}
        \item Creación de un prototipo estático.
        \item Simulación de servicios para obtener la funcionalidad del prototipo estático.
        \item Implementación de los servicios necesarios. 
    \end{enumerate}
    
\end{itemize}

\section{Prototipado evolutivo}\label{proto_evolutivo}
El Modelo de Prototipado Evolutivo es un modelo iterativo a través del cual es posible identificar los requisitos del cliente mediante la construcción de un prototipo inicial. Si el prototipo no se ajusta a las expectativas del cliente se añaden las mejoras necesarias, refinando la versión del prototipo en una nueva iteración. El diseño del prototipo irá evolucionando y ajustándose cada vez más a las necesidades del usuario hasta llegar al sistema final. Este modelo se caracteriza por la forma en la que permite a los desarrolladores crear versiones cada vez más completas del sistema \cite{gomez2012taxonomia} \cite{gamboa2018evolucion}. Este modelo evolutivo itera sobre las etapas de especificación, desarrollo y validación (ver Figura \ref{fig:etapas_evolutivo})

El uso de prototipos se centra en la idea de ayudar a comprender los requisitos que plantea el usuario, sobre todo si este no tiene una idea muy clara de lo que desea. También pueden utilizarse en aquellos casos donde el desarrollador tiene dudas acerca de la viabilidad de ciertas características. Al hacer uso de los prototipos se obtiene una versión temprana de lo que será finalmente el producto, y aunque inicialmente se cree con una funcionalidad reducida, se irá incrementando progresivamente a través de refinamientos sucesivos de las especificaciones del sistema, evolucionando así hasta alcanzar el sistema final \cite{juzgado1996procesos}. 

\begin{figure}[htb]
	\centering
	\includegraphics[height=4.5cm]{figs/metodologia/etapas_evolutivo-crop.pdf} 
	\caption{Etapas del Modelo de Prototipado Evolutivo} 
	\label{fig:etapas_evolutivo}
\end{figure}


\section{Herramientas \textit{software}} 
En esta sección se detallan las herramientas que se han utilizado para el desarrollo de este TFG. A continuación, se clasificarán en diferentes grupos, como planificación y seguimiento, desarrollo, monitorización, gestión de la configuración, documentación y comunicación (ver Figura \ref{fig:herramientas}). 

\begin{figure}[htb]
	\centering
	\includegraphics[height=9cm]{figs/metodologia/herramientas.pdf} 
	\caption{Marco tecnológico} 
	\label{fig:herramientas}
\end{figure}

\subsection{Herramientas de planificación y seguimiento}
\subsubsection{MeisterTask} 
\textit{MeisterTask} \cite{meistertask} es una herramienta en línea que permite que los equipos puedan gestionar sus tareas de manera intuitiva, ofreciendo toda la información que los miembros del equipo necesitan para llevarlas a cabo de un modo eficiente. Con \textit{MeisterTask} el equipo puede organizar y administrar sus tareas en un entorno personalizable que se adapte adecuadamente a las necesidades de los usuarios. Los tableros de proyectos pueden configurarse para admitir flujos de trabajo agilizados, como los \textit{sprints} en el caso del presente TFG.

\subsection{Herramientas de desarrollo}
\subsubsection{Ubuntu 20.04}
\textit{Ubuntu} \cite{ubuntu} es un sistema operativo de \textit{software} libre y de código abierto. Es una distribución de Linux basada en Debian y puede correr tanto en computadores de escritorio como en servidores. Está orientado al usuario promedio por su facilidad de uso y las mejoras que presenta frente a otras distribuciones conforme a la experiencia de usuario. 

\subsubsection{Visual Studio Code}
\textit{Visual Studio Code} \cite{visualstudiocode} es un editor de código fuente desarrollado por \textit{Microsoft} para Windows, Linux y Mac OS. Incluye soporte para la depuración, control integrado de \textit{Git}, resaltado de sintaxis, finalización inteligente de código, fragmentos y refactorización de código. Es gratuito, de código abierto y personalizable. 

\subsubsection{Python}
\textit{Python} \cite{python} es un lenguaje de programación interpretado cuya filosofía es la legibilidad de su código, lo cual ahorra tiempo y recursos facilitando su comprensión e implementación. Se conoce como un lenguaje de programación multiparadigma, ya que soporta parcialmente la orientación a objetos, la programación imperativa y la programación funcional, aunque esta última es soportada en menor medida que las dos anteriores.

\subsubsection{Docker}
\textit{Docker} \cite{docker} es una plataforma \textit{software} que permite crear, probar e implementar aplicaciones rápidamente. Esta herramienta empaqueta el \textit{software} en unidades estandarizadas denominadas contenedores, que incluyen todo lo necesario para que el \textit{software} se ejecute, incluidas las bibliotecas, las herramientas del sistema, el código y el tiempo de ejecución. Los contenedores virtualizan el sistema operativo de un servidor, de manera similar a las máquinas virtuales que virtualizan el \textit{hardware} del servidor (ver Figura \ref{fig:docker_vm}). \textit{Docker} se instala en cada servidor y proporciona simples comandos con los que se pueden crear, iniciar o detener contenedores. 

\begin{figure}[htb]
	\centering
	\includegraphics[height=5.5cm]{figs/metodologia/docker_vm.png} 
	\caption{Comparativa entre \textit{Docker} y una máquina virtual} 
	\label{fig:docker_vm}
\end{figure}

\subsubsection{DeepSpeech}
\textit{DeepSpeech} \cite{deepspeech} es un motor \textit{speech to text} de código abierto que utiliza un modelo entrenado mediante técnicas de aprendizaje automático. El proyecto \textit{DeepSpeech} utiliza \textit{TensorFlow} de \textit{Google} para facilitar la implementación. Asimismo, permite el entrenamiento de nuestro propio modelo para el lenguaje que al usuario mejor le convenga. En el caso del presente \ac{tfg} se ha hecho uso del propio entrenamiento del modelo para el idioma castellano, ya que sólo se encontraba preentrenado el modelo en inglés. 

\subsubsection{Rhasspy}
Como se ha citado anteriormente en el apartado \ref{ap_rhasspy}, \textit{Rhasspy} \cite{rhasspy} es un kit de herramientas destinado a desplegar asistentes de voz, disponible para un amplio abanico de idiomas.

\subsubsection{Ian}
\textit{Ian} \footnote{\url{https://github.com/davidvilla/ian} (visitado 19-05-2021)} es una herramienta que facilita el mantenimiento de paquetes \textit{software} Debian. Se trata de un \textit{frontend} para varias herramientas oficiales de empaquetado de Debian. Esta herramienta ha sido creada por David Villa Alises, profesor titular en la \ac{esi}.  

\subsection{Herramientas de monitorización}
\subsubsection{MQTT}
\ac{mqtt} \cite{mqtt} es un protocolo de transporte de mensajes cliente/servidor para \ac{iot} basado en publicaciones y suscripciones de un canal determinado, denominado \textit{topic}. 

\subsubsection{Eclipse Mosquitto}
\textit{Eclipse Mosquitto} \cite{mosquitto} es un \textit{broker} de mensajes de código abierto implementado por el protocolo \ac{mqtt}. Su objetivo es recibir los mensajes publicados por los clientes y difundirlos entre los clientes que estén suscritos a un \textit{topic} determinado.

\subsubsection{Telegraf}
\textit{Telegraf} \cite{telegraf} es un agente de servidor impulsado por \textit{plugins} para recopilar y enviar datos y eventos de bases de datos, sistemas y sensores \ac{iot}, por lo que es fácilmente extensible. Esta herramienta, al estar escrita en \textit{Go} \cite{go}, se compila solo en un binario que puede ser ejecutado en cualquier sistema sin necesidad de dependencias externas u otras herramientas de gestión de paquetes \textit{software} y requiere una huella de memoria mínima. 

\subsubsection{InfluxDB}
\textit{InfluxDB} \cite{influxdb} es un sistema de gestión de bases de datos desarrollado por la empresa \textit{InfluxData Inc}. Es un \textit{software} de código abierto y se puede usar de manera gratuita, aunque la versión comercial <<InfluxDB Enterprise>> ofrece contratos de mantenimiento y se instala en un servidor dentro de la red de la empresa. Al igual que \textit{Telegraf}, \textit{InfluxDB} está escrito en el lenguaje de programación Go \cite{go}.

\subsubsection{Grafana}
\textit{Grafana} \cite{grafana} es una herramienta de código abierto para el análisis y la visualización de métricas. Se emplea frecuentemente para visualizar de una forma elegante y sencilla series de datos en el análisis de infraestructuras y aplicaciones. 

\subsection{Herramientas de gestión de la configuración}
\subsubsection{Git}
\textit{Git} \cite{git} es un \textit{software} de control de versiones pensado en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente. Esta herramienta permite mantener un historial completo de versiones debido a que está basada en un sistema de trabajo con ramas, las cuales permiten tener, además de la rama principal, otras ramas de progreso donde se realizan los cambios, fusionándose posteriormente con la rama principal. 

\subsubsection{GitHub}
\textit{GitHub} \cite{github} es una plataforma de desarrollo colaborativo donde se alojan proyectos para el desarrollo de \textit{software} y el control de versiones mediante \textit{Git}. El código fuente se encuentra en repositorios almacenados en la nube, proporcionando control de acceso, seguimiento de errores, solicitudes de funciones, gestión de tareas, integración continua y wikis para cada proyecto.  

\subsubsection{Bitbucket}
\textit{Bitbucket} \cite{bitbucket} es una plataforma de desarrollo colaborativo desarrollado por \textit{Atlassian} basado en web, para el desarrollo de los proyectos que utilizan el sistema de control de versiones de \textit{Mercurial} y \textit{Git}. 

\subsection{Herramientas de documentación}
\subsubsection{\LaTeX}
\LaTeX{} es un sistema de composición de textos, orientado a la creación de documentos escritos que presentan una alta calidad tipográfica. Es usado especialmente para la generación de artículos y libros científicos debido a sus características. Es un sistema de código abierto y se centra exclusivamente en el contenido sin tener que preocuparse de los detalles del formato. 

\subsubsection{Overleaf}
\textit{Overleaf} \cite{overleaf} es un editor colaborativo de \LaTeX{} basado en la nube cuya finalidad es escribir, editar y publicar documentos científicos. Además, se asocia con una amplia gama de editores científicos para proporcionar plantillas oficiales de \LaTeX{} para revistas y artículos.

\subsubsection{Draw.io}
\textit{Draw.io} \cite{draw.io} es una aplicación web de creación y modificación de diagramas libre que permite la integración con diversas plataformas. 

\subsubsection{Canva}
\textit{Canva} \cite{canva} es una web online de diseño gráfico y composición de imágenes que presenta herramientas para crear tus propios diseños. Ofrece diversas plantillas gratuitas para diferentes tipos de diseño y también permite realizar nuestros propios diseños desde cero. Las imágenes del presente TFG se han realizado con \textit{Draw.io} y con esta herramienta. 

\subsubsection{Planner 5D}
\textit{Planner 5D} \cite{planner} es una herramienta de construcción y diseño de interiores que permite al usuario crear diseños realistas de interiores del hogar en los modos 2D y 3D.


\subsection{Herramientas de comunicación}
\subsubsection{Slack}
\textit{Slack} \cite{slack} es una plataforma de mensajería basada en canales. Con \textit{Slack} las personas pueden trabajar juntas de manera efectiva, conectar todas sus herramientas y servicios de \textit{software} y encontrar la información que necesiten, todo ello dentro de un entorno seguro de nivel empresarial. 

\subsubsection{Microsoft Teams}
\textit{Microsoft Teams} \cite{teams} es una plataforma basada en la nube cuyo principal objetivo es la colaboración en equipo. Pertenece a los productos de \textit{Microsoft} y su principal función es ser una herramienta de mensajería empresarial que permite la comunicación y la colaboración en tiempo real entre usuarios de dentro y fuera de la organización. 

\section{Herramientas \textit{hardware}}
Para el desarrollo del presente \ac{tfg} se ha utilizado el siguiente material \textit{hardware}:
\begin{itemize}
    \item Ordenador portátil: MSI GP63 Leopard 8RD; procesador Intel Core i7 de octava generación; gráfica Nvidia GeForce GTX 1050 Ti; disco duro \acs{ssd} de 256 GB; disco duro \acs{hdd} de 1 TB; 16 GB de memoria \acs{ram}.
    \item Ordenador de sobremesa: procesador Intel Core i7 de tercera generación; disco duro \acs{hdd} de 1 TB; 8 GB de memoria \acs{ram}.
    \item Raspberry Pi 4: procesador Quad core Cortex-A72 de cuatro núcleos a 1,5 GHz; 4 GB de memoria \acs{ram}.
    \item Lector \acs{rfid} ACR122U\footnote{\href{https://www.sticard.com/wp-content/uploads/2014/04/lectores-RFID-NFC-ACS-ACR122U.pdf}{Especificaciones del lector RFID} (visitado 18-06-2021)}.
    \item Lector \acs{rfid} con módulo \textit{WiFi} creado por el Grupo de Investigación \ac{arco}.
    \item Sensor de puertas y ventanas Xiaomi MiJia \footnote{\label{MiJia}\href{https://i01.appmifile.com/webfile/globalimg/Global_UG/Mi_Ecosystem/Mi_Smart_Sensor_Set/es-ES_V1.pdf}{Manual de usuario de los sensores Xiaomi MiJia} (visitado 20-05-2021)}.
    \item Sensor de movimiento Xiaomi MiJia $^\textup{\scriptsize{\ref{MiJia}}}$.
    \item Sensor de temperatura y humedad Xiaomi Aqara\footnote{\href{https://files.miot-global.com/files/manuals\%20updated/Temperature\%20and\%20Humidity\%20Sensor\%20Quick\%20Start\%20Guide.pdf}{Manual de usuario del sensor de temperatura y humedad Xiami Aqara} (visitado 20-05-2021)}.
    \item Zig-a-zig-ah! (zzh!).
\end{itemize}