*Ve marcando con una X lo que ya hayas apañado/considerado*

- [X] Me chirría un poco que a lo largo de los antecedentes tengas tan pocas referencias. En la sección 3.4, por ejemplo, que mencionas los estándares, las frecuencias en las que funciona cada tecnología, etc., ¿no has consultado nada para contar todo eso? He visto que al principio de la sección tienes varias referencias. ¿Se supone que a partir de esas referencias has escrito el resto de subsecciones? De ser así, deberías volver a poner la referencia después, allá donde hayas escrito algo que hayas sacado de esa fuente.

- [X] Ya te lo he puesto por ahí en algún comentario, pero asegúrate en todo el doc de que después de los dos puntos siempre pones minúscula.