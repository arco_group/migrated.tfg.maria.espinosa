\chapter{Introducción}
\label{cap:Introduccion}

\drop{D}ebido a las mejoras y a los avances que se producen día a día en diferentes ámbitos de nuestra vida se está dando como resultado un incremento en la esperanza de vida de la población, siendo el envejecimiento de la población española una realidad muy bien conocida. Esto produce un cambio demográfico y el crecimiento de problemas de salud y bienestar en aquellas personas con mayor edad. Se considera que una persona pertenece al grupo de la tercera edad, según el \ac{imserso} \cite{imserso}, cuando tiene 65 años o más. Conforme indica el \ac{ine} \cite{INE}, se ha producido un incremento en el porcentaje de la población de la tercera edad en los últimos cinco años desde el 18,5\% en el 2015 hasta el 19,58\% en el 2020 del total de la población española. La Figura \ref{fig:proporcion_mayores} muestra la proporción, en porcentaje, de personas de la tercera edad por comunidad autónoma en el año 2020 según el \ac{ine}. 

\begin{figure}[htb]
	\centering
	\includegraphics[height=7.5cm]{figs/introduccion/proporción_personas_mayores_CCAA.pdf} 
	\caption{Proporción de personas mayores por comunidad autónoma en 2020} 
	\label{fig:proporcion_mayores}
\end{figure}

A raíz de este crecimiento se estima que en un futuro las personas mayores formarán la mayor parte del territorio y ocuparán el mayor porcentaje de la población en la pirámide demográfica. Esto hace que problemas como la pérdida de relaciones sociales o de interacción con el medio, característicos en este grupo de personas, se hayan convertido en una de las principales preocupaciones de la sociedad \cite{fernandez2020soledad}. En el último año ha incrementado el número de personas mayores que viven solas, pasando de 2.009.100 personas en el 2019 a 2.131.400 en el 2020 \cite{INE2}. En vista de esta situación, actualmente existe un creciente interés en desarrollar tecnologías y dispositivos que promuevan y permitan vivir de forma independiente a este grupo de la población durante el mayor tiempo posible. Este interés se debe a que un estudio en \cite{bayer2000fixing} muestra que a la pregunta <<¿le gustaría quedarse en su actual residencia el mayor tiempo posible?>> el 71\% de las personas mayores estaban muy de acuerdo y el 12\% estaban algo de acuerdo, frente al 7\% que estaban algo en desacuerdo, al 8\% que estaban muy en desacuerdo y al 1\% que no sabían qué respuesta proporcionar, siendo en mayor porcentaje el número de personas mayores que prefieren residir en su vivienda el mayor tiempo posible. En consecuencia, las tecnologías para la domotización y la monitorización automática del entorno doméstico son soluciones bien valoradas a la hora de afrontar aquellos problemas derivados del envejecimiento de la población \cite{yanco2002automation}.

\section{Domótica y sensórica}
El concepto de \textit{domótica} se considera la evolución lógica de la instalación eléctrica, que incorpora al equipamiento de las viviendas y edificios una sencilla tecnología, la cual permite gestionar de forma enérgicamente eficiente, segura y cómoda para el usuario los distintos aparatos e instalaciones domésticas tradicionales que conforman una vivienda.
La domótica surgió a raíz del protocolo de comunicación X10, que fue desarrollado por \textit{Pico Electronics of Glenrothes} entre 1976 y 1978 para habilitar el control remoto de los dispositivos domésticos compatibles con el protocolo a través del cableado eléctrico del hogar \cite{x10}. Con el avance de la electrónica y de la informática este concepto empezó a crecer, llegando a España entre los años noventa y 2000 \cite{cuevas2002protocolo}. 


Pero, ¿qué aporta realmente la domótica? Según la \ac{cedom}\footnote{\url{http://www.cedom.es/sobre-domotica/que-es-domotica} (visitado 22-02-2021)}, la domótica permite, a través de las tecnologías aplicadas al control y a la automatización del hogar, una gestión eficiente del uso de la energía aportando seguridad, comodidad y comunicación entre el usuario y el sistema. Además, contribuye a mejorar la calidad de vida del usuario en diferentes aspectos:

\begin{itemize}
    \item Facilita el ahorro energético, consiguiendo una gestión eficiente de los dispositivos. 
    \item Fomenta la accesibilidad al hacer posible el manejo de los dispositivos domésticos para que se ajusten a las necesidades de aquellas personas que lo precisen, como personas mayores o con discapacidad. 
    \item Aporta seguridad a través de la vigilancia para personas, animales o bienes.
    \item Proporciona comodidad debido a que es posible gestionar los dispositivos y las actividades domésticas. 
    \item Permite la comunicación con el usuario, ya que tiene el control remoto y la supervisión de su vivienda a través de su teléfono móvil o su ordenador personal. 
\end{itemize}

Para la monitorización automática de la salud se puede hacer uso de la sensórica y de los sistemas de comunicación. El concepto de \textit{sensórica} se refiere a la tecnología que abarca cualquier tipo de sensor, aunque el enfoque del presente \ac{tfg}  tiene en cuenta los sensores inteligentes del Internet de las Cosas o \textit{\ac{iot}}. Debido a la evolución de los sistemas \ac{iot} se ha vuelto imprescindible la obtención de datos para poder realizar acciones autónomas y extraer conclusiones sobre problemas latentes, como el exceso de temperatura en una sala del domicilio o la intrusión en el hogar. Estas herramientas de domotización y monitorización tienen una doble función:
\begin{itemize}
    \item Aumenta la seguridad y la protección de las personas mayores que viven de manera independiente. 
    \item Permite a las personas mayores aprovechar su autonomía durante un periodo más largo de tiempo, llegando a mejorar su calidad de vida. 
\end{itemize}

Esta automatización de la vivienda recibe el nombre de hogar inteligente o \textit{Smart Home}. Este concepto al principio se utilizaba para controlar sistemas ambientales, como la iluminación o la calefacción, pero con el reciente uso de la tecnología doméstica inteligente se ha ido avanzando hasta conseguir el control del entorno en el hogar.
Además, se ha demostrado que las tecnologías del hogar inteligente han alcanzado un buen estado de madurez, lo que implica una mejora en las mismas \cite{ricquebourg2006smart}.

\section{Envejecimiento activo y saludable}
La intención de proporcionar unas condiciones óptimas para cubrir las necesidades de las personas mayores en su proceso de envejecimiento, mediante soluciones basadas en la tecnología, pretende ser abordada desde el paradigma denominado Envejecimiento Activo y Saludable o \textit{Active and Healthy  Ageing}. Aunque décadas antes se habían realizado estudios sobre cómo <<envejecer bien>> o el envejecimiento positivo, el término \textit{Healthy and Active Ageing} fue definido por primera vez en 2002 por la \ac{oms} en \cite{world2002active}. En el folleto publicado se define el envejecimiento activo como <<el proceso de optimizar las oportunidades de salud, participación y seguridad con el fin de mejorar la calidad de vida a medida que las personas envejecen>>. El envejecimiento activo y saludable puede considerarse como un objetivo global y un concepto político \cite{walker2008commentary}. A nivel poblacional, la \ac{oms} estableció seis determinantes o influencias principales del envejecimiento activo y saludable \cite{world2002active}: determinantes de comportamiento, personales, de servicios sanitarios y sociales, económicos, físicos y sociales. 
Junto a estos seis determinantes principales, existen dos determinantes transversales: la cultura y el género (ver Figura \ref{fig:det_envejecimiento}).

\begin{figure}[htb]
	\centering
	\includegraphics[height=5.3cm]{figs/introduccion/determinantes_envejecimiento_activo.pdf} 
	\caption{Determinantes del envejecimiento activo y saludable \cite{world2002active}} 
	\label{fig:det_envejecimiento}
\end{figure}


\section{Estructura del documento}
La estructura del presente documento es la siguiente:

\begin{itemize}
    \item \textit{Capítulo \ref{cap:Introduccion}: Introducción}
    
    Presente capítulo del documento. Se plantea el contexto del \ac{tfg} y la cuestión para la que se pretende ofrecer una solución. 

    \item \textit{Capítulo \ref{cap:Objetivos}: Objetivos}
    
    En este capítulo se describe el objetivo principal del presente \ac{tfg}, así como los objetivos específicos necesarios para su consecución.
    
    \item \textit{Capítulo \ref{cap:Antecedentes}: Estado del arte}
    
    En este capítulo se pone en contexto las diferentes tecnologías que, como servicios de un entorno de envejecimiento activo y saludable, serán utilizadas para validar la pasarela inteligente propuesta en este trabajo. Así, se hace una breve introducción al concepto de asistente de voz, su origen y su cronología hasta la actualidad. Además, se presenta el estudio realizado sobre varias alternativas de asistentes de voz existentes en el marco actual. Por otro lado, se analizan diferentes tecnologías de comunicación inalámbrica utilizadas en la actualidad para el sistema de monitorización del entorno y del comportamiento de la persona mayor. Estas tecnologías heterogéneas serán integradas mediante la pasarela propuesta en este proyecto, demostrando así su verdadero potencial. 
    
    \item \textit{Capítulo \ref{cap:Metodologia}: Metodología}

    En este capítulo se expone la metodología de desarrollo utilizada en el presente \ac{tfg}. Además, se muestran las herramientas \textit{software} y \textit{hardware} que han sido empleadas en el desarrollo.  
    
    \item \textit{Capítulo \ref{cap:Resultados}: Resultados}
    
    En este capítulo se presentan los prototipos por los que ha pasado el sistema a lo largo de su desarrollo, abarcando también los servicios que han sido utilizados para validar la pasarela propuesta (tanto del asistente de voz como de la monitorización del entorno y del comportamiento de la persona de la tercera edad). Asimismo, se muestran los resultados conseguidos de los sistemas una vez se han obtenido los productos finales.   
    
    \item \textit{Capítulo \ref{cap:Conclusiones}: Conclusiones}
    
    En este capítulo se exponen las conclusiones de los resultados obtenidos, evaluando si se han cumplido los objetivos propuestos y se indican cuáles son las competencias adquiridas. Posteriormente se exponen el trabajo derivado y el trabajo futuro relacionado con el proyecto. Por último, se presentan los conocimientos finales adquiridos a lo largo del desarrollo del \ac{tfg} y la opinión personal de la autora. 
\end{itemize}

Seguidamente se proporciona la bibliografía utilizada en el presente \ac{tfg}. Finalmente, se presentan los anexos, donde se pueden consultar los detalles técnicos sobre algunas herramientas utilizadas para el desarrollo de este \ac{tfg}. Estos anexos son los siguientes: 

\begin{itemize}
    \item \textit{Anexo \ref{cap:AnexoA}:} se muestran los pasos a seguir para entrenar un modelo de reconocimiento de voz y utilizarlo con \textit{DeepSpeech}.
    \item \textit{Anexo \ref{cap:AnexoB}:} se muestra la plantilla del formulario de consentimiento para los participantes en la investigación.
    \item \textit{Anexo \ref{cap:AnexoC}:} se muestran los pasos a seguir para configurar el coordinador Zig-ah-zig-ah! o zzh! 
    \item \textit{Anexo \ref{cap:AnexoD}:} se muestran los pasos a seguir para llevar a cabo la instalación y la ejecución del paquete \textit{software} Debian desarrollado para la monitorización del entorno y del comportamiento de la persona mayor en el hogar.
\end{itemize}
 




