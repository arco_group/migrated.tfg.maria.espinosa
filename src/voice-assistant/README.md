## Receta del cliente *Rhasspy*

### Instalación
Como primer paso antes de utilizar el cliente de *Rhasspy* es necesario instalar la librería cliente para comunicarse con el servidor remoto de *Rhasspy*
```shell
pi@raspberrypi:~$ pip3 install rhasspy-client
```

### Ejecución
Una vez realizada la instalación de la librería necesaria, pasamos a la ejecución del cliente *Rhasspy*. Para ello el único paso que hay que seguir es ejecutar el cliente con la entrada de audio. 

Existen dos formas de hacerlo, ejecutando el cliente para que recoja como entrada de audio la voz a tiempo real del usuario 

```shell
pi@raspberrypi:~$ python3 rhasspy-client record
```

o introduciendo por línea de argumentos el archivo de audio con formato *.wav* 
```shell
pi@raspberrypi:~$ python3 rhasspy-client speech-to-text <nombre_audio>.wav
```