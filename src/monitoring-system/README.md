## Receta del paquete *software* Debian

### Instalación 
Como primer paso se añade el repositorio del grupo ARCO
``` shell
pi@raspberrypi:~$ sudo apt-key adv --fetch -keys https://uclm-arco.github.io/debian/uclm-arco.asc
pi@raspberrypi:~$ echo "deb https://uclm-arco.github.io/debian/ sid main" | sudo tee /etc/apt/sources.list.d/arco.list
pi@raspberrypi:~$ sudo apt update
```

El paquete `shapes-monitoring-service` contiene dependencias para instalar *Grafana*, por lo que antes de instalar el paquete de la monitorización es necesario añadir el repositorio de *Grafana* en el sistema

```shell
pi@raspberrypi:~$ sudo apt-get install -y apt-transport-https
pi@raspberrypi:~$ sudo apt-get install -y software-properties-common wget
pi@raspberrypi:~$ wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
pi@raspberrypi:~$ echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
pi@raspberrypi:~$ sudo apt update
pi@raspberrypi:~$ sudo apt-get install grafana
```

Posteriormente instalamos el cliente de la base de datos *InfluxDB*
```shell
pi@raspberrypi:~$ sudo apt-get install influxdb
```

Para finalizar se instala el paquete que contiene los archivos de configuración de los servicios que forman el sistema de monitorización del entorno y del comportamiento de la persona en su hogar

```shell
pi@raspberrypi:~$ sudo apt-get install shapes-monitoring-service
```
y la librería `paho-mqtt` ya que se utiliza en el cliente MQTT
```shell
pi@raspberrypi:~$ pip3 install paho-mqtt
```

### Ejecución 
Considerando que la instalación se ha llevado a cabo correctamente pasamos a ejecutar el sistema de monitorización. Para ello, primero generamos los árboles de dependencia existentes en `systemd`
```shell
pi@raspberrypi:~$ sudo systemctl daemon-reload
```

y posteriormente iniciamos el sistema de monitorización y el servidor de *Grafana*
```shell
pi@raspberrypi:~$ sudo systemctl start shapes-monitoring.service
pi@raspberrypi:~$ sudo systemctl start grafana-server.service
```

Si el usuario desea que los servicios se inicien siempre se ejecutan los siguientes comandos 

```shell
pi@raspberrypi:~$ sudo systemctl enable shapes-monitoring.service
pi@raspberrypi:~$ sudo systemctl enable grafana-server.service
```

Una vez que el servicio se está ejecutando comprobamos la conexión con la base de datos *InfluxDB* y creamos el *database* necesario para la recogida de las métricas que publican los sensores inteligentes 
```shell 
pi@raspberrypi:~$ sudo shapes-monitoring -u
Connected to http://localhost:8086 version 1.8.5
    InfluxDB shell version: 1.6.4
    > create database zigbee2mqtt
    > use zigbee2mqtt
    Using database zigbee2mqtt
    > create user telegraf with password 'telegraf' with all privileges
    > grant all on zigbee2mqtt to telegraf
    > exit
Database checked!
Done updating!
```

Si desea visualizar la información que contiene el *dashboard* que se mostrará en *Grafana* ejecute
```shell
pi@raspberrypi:~$ sudo shapes-monitoring -s
INFO: Datasource provider file at /etc/grafana/provisioning/datasources/shapes-monitoring-datasource.yaml
INFO: Dashboard provider file at /etc/grafana/provisioning/dashboards/shapes-monitoring-dashboard.yaml
INFO: Found dashboard definition at /var/lib/grafana/dashboards/shapes-monitoring-dashboard.json:
INFO: 	Name: smart-sensors
INFO: 	UID: fFBmCCrMz
INFO: Available panels:
INFO: 	ID: 1	Description: Bathroom sensors
INFO: 	ID: 2	Description: Living room sensors
INFO: 	ID: 3	Description: Bedroom sensors
INFO: 	ID: 4	Description: Kitchen sensors
```

Seguidamente lance el cliente MQTT y comenzará a mostrar las métricas que transmiten los sensores, las cuales se publicarán posteriormente en un determinado *topic* para que el agente *Telegraf* recoja esas métricas y las almacene en la base de datos *InfluxDB*

```shell
pi@raspberrypi:~$ shapes-monitoring-client
Connected to MQTT Broker localhost:1883 with return code 0!
Received {"battery":100,"contact":true,"linkquality":162,"voltage":3005} from zigbee2mqtt/door_window_living-room/SENSOR topic!
Sensor data: {'battery': 100, 'contact': 1, 'linkquality': 162, 'voltage': 3005}
Sensor data published to zigbee2mqtt/smart-sensors/living-room topic!

Received {"battery":100,"contact":true,"linkquality":158,"voltage":3005} from zigbee2mqtt/door_window_bedroom/SENSOR topic!
Sensor data: {'battery': 100, 'contact': 0, 'linkquality': 158, 'voltage': 3005}
Sensor data published to zigbee2mqtt/smart-sensors/bedroom topic!

Received {"battery":100,"contact":false,"linkquality":129,"voltage":3015} from zigbee2mqtt/door_window_living-room/SENSOR topic!
Sensor data: {'battery': 100, 'contact': 0, 'linkquality': 129, 'voltage': 3015}
Sensor data published to zigbee2mqtt/smart-sensors/living-room topic!

Received {"battery":100,"humidity":52.09,"linkquality":129,"pressure":947.6,"temperature":25.24, "voltage":3015} from zigbee2mqtt/temperature_humidity_living-room/SENSOR topic!
Sensor data published to zigbee2mqtt/smart-sensors/living-room topic!
```

Siguiendo estos pasos ya está todo listo para introducirse en la [interfaz gráfica](https://localhost:3000) de *Grafana* y visualizar las métricas recogidas sobre el entorno del hogar y el comportamiento de la persona en su vivienda. 

